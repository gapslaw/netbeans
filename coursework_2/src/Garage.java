/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hugocarvalho
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/*
*author @ Hugo Carvalho
*/
public class Garage {
    
    public static void main(String[] args){
        
        // create JFrame and a JTable 
        JFrame frame = new JFrame();
        JTable table = new JTable();

        // create a table headers for the jobs 
        Object[] columns = {"Id","Vehicle Make","Type of Repair","Mechanic","Rover Parts", "Ford Parts"};
        
        //Novice(Paul), Workman(John), Expert(Carlos)
        
        Object[][] data = {{"1", "Ford",
     "New Exhaust","John", "4 Exhausts","3 Exhausts"}, {"2", "Rover",
     "New Exhaust","Carlos","3 Engines", "2 Engines"},{"3", "Rover",
     "New Tyres","Carlos", "4 sets of tyres", "2 sets of tyres"},{"4", "Ford",
     "New Engine","Carlos"},{"5", "Ford",
     "New Engine","Carlos"},{"6", "Rover",
     "New Tyres","Paul"},{"7", "Rover",
     "New Tyres","Paul"}, {"8", "Ford",
     "New Tyres","John"}, {"9", "Ford",
     "New Exhaust","Carlos"}, {"10", "Rover",
     "New Tyres","Paul"}};
        
        DefaultTableModel model = new DefaultTableModel(data, columns);
        model.setColumnIdentifiers(columns);
        
        // set the model to the table
        table.setModel(model);
        
        // Change A JTable Background Color, Font Size, Font Color, Row Height
        table.setBackground(Color.LIGHT_GRAY);
        table.setForeground(Color.black);
        Font font = new Font("",1,22);
        table.setFont(font);
        table.setRowHeight(30);

        
        // create JTextFields
        JTextField textId = new JTextField("Enter ID");
        JTextField textVmake = new JTextField("Make");
        JTextField textTrepair = new JTextField("repair");
        JTextField textMechanic = new JTextField("Mechanic");
      
        
        // create JButtons
        JButton btnBook = new JButton("Book");
        JButton btnCancel = new JButton("Cancel job");
        JButton btnAssign = new JButton("Assign job");
        
        
        //text fields sizes
        textId.setBounds(20, 220, 100, 25);
        textVmake.setBounds(20, 250, 100, 25);
        textTrepair.setBounds(20, 280, 100, 25);
        textMechanic.setBounds(20, 310, 100, 25);
 
        
        //buttons sizes
        btnBook.setBounds(150, 220, 100, 25);
        btnAssign.setBounds(150, 265, 100, 25);
        btnCancel.setBounds(150, 310, 100, 25);
       
        
        // create JScrollPane
        JScrollPane pane = new JScrollPane(table);
        pane.setBounds(0, 0, 1100, 200);

        frame.setLayout(null);
        frame.add(pane);
        
        // add JTextFields to the jframe
        frame.add(textId);
        frame.add(textVmake);
        frame.add(textTrepair);
        frame.add(textMechanic);
        
        // add JButtons to the jframe
        frame.add(btnBook);
        frame.add(btnCancel);
        frame.add(btnAssign);
      
        
        // create an array of objects to set the row data
        Object[] row = new Object[4];
        
        
        // button add row
        btnBook.addActionListener((ActionEvent e) -> {
            row[0] = textId.getText();
            row[1] = textVmake.getText();
            row[2] = textTrepair.getText();
            row[3] = textMechanic.getText();
            
            // add row to the model
            model.addRow(row);
        });
        
        // button remove row
        btnCancel.addActionListener((ActionEvent e) -> {
            // i = the index of the selected row
            int i = table.getSelectedRow();
            if(i >= 0){
                // remove a row from jtable
                model.removeRow(i);
            }
            else{
                System.out.println("Cancellation Error");
            }
        });
        
        // get selected row data From table to textfields 
        table.addMouseListener(new MouseAdapter(){
        
        @Override
        public void mouseClicked(MouseEvent e){
            
            // i = the index of the selected row
            int i = table.getSelectedRow();
            
            textId.setText(model.getValueAt(i, 0).toString());
            textVmake.setText(model.getValueAt(i, 1).toString());
            textTrepair.setText(model.getValueAt(i, 2).toString());
            textMechanic.setText(model.getValueAt(i, 3).toString());
            
        }
        });
        
        // button update row
        btnAssign.addActionListener((ActionEvent e) -> {
            // i = the index of the selected row
            int i = table.getSelectedRow();
            
            if(i >= 0)
            {
                model.setValueAt(textId.getText(), i, 0);
                model.setValueAt(textVmake.getText(), i, 1);
                model.setValueAt(textTrepair.getText(), i, 2);
                model.setValueAt(textMechanic.getText(), i, 3);
                
            }
            else{
                System.out.println("Assigning Job Error!");
            }
        });
        
        frame.setSize(1100,500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
    }
}
